
# Student Management System

## Technical Aspects
    
|                                   | Version/Info      |
| ---------------------------------:|:-----------------:|
| Java Development Kit              | 17                |
| Database                          | MySQL             | 
| mysql-connector-java-8.0.28.jar   | 12.18.4           |
| jstl-impl.jar                     | JSTL 1.2.1        |
| jstl-api.jar                      | JSTL 1.2.1        |
| Apache Tomcat or TomEE            | 8.5.65            |
| Frontend Template => Core-UI      | [View Template](https://coreui.io/demo/4.0/free/)|
+  This project has been built in Apache Netbeans IDE 12.6.

### Project Setup

#### Install JDK and run project

+ For downloading jdk visit [Download JDK](https://www.oracle.com/java/technologies/downloads/).
+ For downloading Apache Netbeans visit [Download Netbeans](https://netbeans.apache.org/download/archive/index.html).
+ Clone the project from the [Project Repo](https://gitlab.com/mrtech.shashi/student-management-system.git)
+ Go to the project.
+ Checkout to master branch if any others.
+ Open the project in the IDE.
+ Fix the missing dependencies if any.
+ Right click on the project and click on run to run the project.

#### Database

+ The sql file is included in the project so download it.
+ Create a database named student_management_system.
+ Import the downloaded sql file.

#### Packages Used by Template

+ "dependencies": {
    "@coreui/chartjs": "^3.0.0",
    "@coreui/coreui": "^4.1.1",
    "@coreui/icons": "^2.1.0",
    "@coreui/utils": "^1.3.1",
    "chart.js": "^3.7.1",
    "simplebar": "^5.3.6"
  }

## Project Overview
A simple student management system where the information of the student will be stored in the database and will also perform display, delete and edit. As well as the same operations for course too. The password encryption has also been implemented using md5 algorithm.

### Scope Breakdown
The main agenda for building this system is to showcase the CRUD operation and template slicing.

##### The Project Requirement has been broken down on the basis of same agenda.

+ Add Student
    - Student detail form
    - Check if email already exists during form submission. 
    - Copyright Disclaimer

+ Display Student (can be accessed through sidebar)
    - Student detail in the table.
    
+ Add Course
    - Course detail form

+ Display Course
    - List of the course that has been added.

+ Login Page
    - The default user is `admin` and password is `12345678`.

### Further help
To get any help or had any queries do contact me via mail [Shashi Bhandari](https://www.gmail.com) 

`email`: shasheebhandari@gmail.com
